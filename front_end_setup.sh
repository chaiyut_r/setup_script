#!/bin/bash

# Pre-install install 
sudo apt update
sudo apt install -y wget
sudo apt install -y unzip

# Install nodejs
function nodejs {
  curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install -y nodejs  
}

# Install react-native
function react_native {
	npm install -g react-native-cli
}

# Install andriod studio
function android_studio {
	sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386
	wget https://dl.google.com/dl/android/studio/ide-zips/2.2.3.0/android-studio-ide-145.3537739-linux.zip
	sudo unzip android-studio-ide-145.3537739-linux.zip /opt/
	cd /opt/android-studio/bin/ && ./studio.sh
}

# Install editer 
function editer {
	if [ $1 = "-a" ] || [ $1 = "--atom" ]; then
		wget https://atom.io/download/deb
		sudo dpkg -i $(ls -a | grep "atom")
		sudo apt-get install -f
	elif [ $1 = "-s" ] || [ $1 = "--sublime" ]; then
		wget https://download.sublimetext.com/sublime-text_build-3126_amd64.deb
		sudo dpkg -i $(ls -a | grep "sublime")	
		sudo apt-get install -f
	else
		wget https://az764295.vo.msecnd.net/stable/ee428b0eead68bf0fb99ab5fdc4439be227b6281/code_1.8.1-1482158209_amd64.deb
		sudo dpkg -i $(ls -a | grep "code")
		sudo apt-get install -f
	fi
}			 

nodejs
react_native
editer $1
android_studio
