#!/bin/bash
codename=$(lsb_release -c | sed 's/.*://' | awk '{print $1}')
if [ $codename = "freya" ]; then
  repo="deb https://apt.dockerproject.org/repo ubuntu-trusty main"
elif [ $codename = "loki" ]; then
  repo="deb https://apt.dockerproject.org/repo ubuntu-xenial main"
else
  repo="deb https://apt.dockerproject.org/repo ubuntu-$codename main"
fi

uname -r
# Update your apt sources
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates
sudo apt-key adv \
             --keyserver hkp://ha.pool.sks-keyservers.net:80 \
             --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo $repo | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
apt-cache policy docker-engine

# Prerequisites by Ubuntu Version
sudo apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get install -y docker-engine
sudo service docker start

# Manage Docker as a non-root user
sudo groupadd docker
sudo usermod -aG docker $USER
